#!/usr/bin/python

# this is the re-staging script for branches that are on the 16.1R2 or 20.2 software
# Tested on: 16.1r2S11, 20.2.3
version = 1.11
# Changelog:
#   Version 1.11
#       - modified check to ensure that rollback snapshot actually exists to use get on the dict rather than direct
#         access due an issue where FlexVNF has <nil> in the snapshot discription
#   Version 1.10
#       - added check to ensure that rollback snapshot actually exists
#   Version 1.9
#       - fixed issue where phase3 was occurring after phase2 rollback
#   Version 1.8
#       - fixed issue in rollback where was calling gethostbyname() instead of gethostname()
#       - fixed issue in ping_default_gw where it was executing execute_command in place of execute_cli_command
#       - changed exception handling in ping_default_gw to type Exception
#   Version 1.7
#       - fixed an issue in verify_versa_services
#   Version 1.6:
#       - merged the python2 and python3 versions into a single script
#       - SMTP support (Alpha quality, not for production use
#   Version 1.5:
#       - added additional debug logging
#   Version 1.4:
#       - added check for scheduled command after erasing configuration due to 21.1.x nuking scheduled task
#   Version 1.3:
#      - Collect diagnostic data prior to rollback
#      - Now supports ability to change soft serial number
#   Version 1.2:
#      - No change, upreved to match corresponding 21.x.x release
#      - Updated to support 21.2.2+ versions of FlexVNF
#   Version 1.1:
#      - Fix for vsh status outputting a subjugated message
#      - addition of 18.04 support for FlexVNF
#   Version 1.0:
#      - Initial release
import os
import logging
import datetime
from pwd import getpwnam
import grp
import sys
import argparse
import pickle
import base64
from tempfile import NamedTemporaryFile
import json
import time
import re
import xml.etree.ElementTree as ET
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import socket
import threading

if sys.version_info[0] == 2:  # we are using python2
    import ConfigParser as configparser
    import imp
    staging_script_path = '/opt/versa/scripts/staging.py'
    staging_script = imp.load_source('staging', staging_script_path)
elif sys.version_info[0] == 3:  # we are using python3
    from pathlib import Path
    import configparser

    try:
        # this is for the pyinstaller
        staging_script_path = os.path.join(sys._MEIPASS, "files/staging.py")
    except AttributeError:
        # this is for the standalone script
        staging_script_path = '/opt/versa/scripts/staging.py'

    if not os.path.exists(staging_script_path):
        print("error: staging script not found at {} exiting".format(staging_script_path))
        sys.exit(-1)
    if sys.version_info.major == 3:
        if 3 <= sys.version_info.minor <= 4:
            from importlib.machinery import SourceFileLoader

            staging_script = SourceFileLoader("module.name", staging_script_path).load_module()
        elif sys.version_info.minor >= 5:
            import importlib.util

            spec = importlib.util.spec_from_file_location("module.name", staging_script_path)
            staging_script = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(staging_script)
        else:
            print("error: invalid python version detected, must be >3.3: {}".format(sys.version_info))
            sys.exit(-1)
else:
    print("invalid python version detected, exiting!!")
    sys.exit(-1)


# global defines
serial_file_path = "/var/lib/vs/.serial"
g_job = ""
g_command = ""
g_delay = 5
g_scheduled_task_id = None
packages_path = "/home/versa/packages/"
upgrade_log_path = "/var/log/versa/upgrade.log"
package_file_path = ""
download_started = download_completed = download_detection_completed = False
upgrade_started = upgrade_detection_completed = False
smtp_config_file_path = "./restage_smtp.ini"

class OwnedFileHandler(logging.FileHandler):
    """Create a log file owned by user:group"""

    def __init__(self, filename, mode='a', encoding=None, delay=False, uid=None, gid=None):
        logging.FileHandler.__init__(self, filename, mode=mode, encoding=encoding, delay=delay)
        if os.path.exists(filename) and uid and gid:
            os.chown(filename, uid, gid)


def deserialize(base64_string):
    """Return an object from a pickled string"""
    base64_bytes = base64_string.encode("utf-8")
    pickle_bytes = base64.b64decode(base64_bytes)
    obj = pickle.loads(pickle_bytes)
    return (obj)


def serialize(obj):
    """Return a string representation of this object"""
    pickle_string = pickle.dumps(obj)
    base64_bytes = base64.b64encode(pickle_string)
    base64_string = base64_bytes.decode('utf-8')
    return (base64_string)


def execute_process(command):
    result = os.popen('/bin/bash -c ' + "\'" + command + "\'").read()
    log("result = {}".format(result), terminal=False)
    return result


def execute_cli_command(command):
    # NOTE: __CLI is polymorphically defined in the base classes as self._VersaDevice__CLI
    __CLI = '/opt/versa/confd/bin/confd_cli -u versa'
    command = """\" {}\"""".format(command)
    command = __CLI + " <<< " + command
    log("executing cli command: {}".format(command), terminal=False)
    result = os.popen('/bin/bash -c ' + "\'" + command + "\'").read()
    inconsistent_db_message = "result: \n--- WARNING ------------------------------------------------------\nRunning db may be inconsistent. Enter configuration mode and\nload a saved configuration.\n------------------------------------------------------------------\n"
    result = result.replace(inconsistent_db_message, "")
    log("result: {}".format(result), terminal=False)
    return result


def schedule_command(command, delay_minutes=0):
    global g_command, g_job, g_scheduled_task_id
    log('scheduling {} for execution in {} minutes'.format(command, delay_minutes), terminal=False)
    f = NamedTemporaryFile(delete=False)
    f.write(command.encode())
    f.close()
    command = "sudo at -f {} now + {} min 2>&1".format(f.name, delay_minutes)
    output = execute_process(command)
    if "job" in output:
        g_job = output
        # check_scheduled_command(output, command, delay_minutes)
        log("command {} successfully scheduled at: {}".format(command, delay_minutes), terminal=False)

        if 'warning: commands will be executed using /bin/sh' in output:
            output = output.split('\n')[1]
        g_scheduled_task_id = output.split()[1]
        log("scheduled task id: {}".format(g_scheduled_task_id))
        ret = True
    else:
        log("command {} not successfully scheduled".format(command), terminal=False)
        ret = False
    os.unlink(f.name)
    return ret


def check_scheduled_command():
    global g_command, g_job
    task_id = g_job.split(" ")[7]
    log('check_scheduled_command looking for task id: {}'.format(g_scheduled_task_id))
    atq_command = "sudo atq"
    output = execute_process(atq_command)
    if task_id not in output:
        log("task id {} not present, attempting to reschedule command".format(g_scheduled_task_id))
        schedule_command(g_command, 5)
    else:
        log('task id {} still scheduled'.format(g_scheduled_task_id))


def delete_scheduled_command():
    """delete a scheduled command from the at queue"""
    global g_scheduled_task_id
    log('attempting to delete scheduled task: {}'.format(g_scheduled_task_id))
    if g_scheduled_task_id:
        command = "atrm {}".format(g_scheduled_task_id)
        output = execute_process(command)
        if "Cannot find jobid {}".format(g_scheduled_task_id) in output:
            log('Failed to delete scheduled task: task did not exist')
            return False
        log("Successfully deleted at task: {}".format(g_scheduled_task_id))
        return True
    else:
        log("Scheduled task is None, skipping delete")
        return False


def get_virtual_router_name():
    smtp_config = configparser.ConfigParser()
    smtp_config.read(smtp_config_file_path)
    system1_vrf = smtp_config.get('SMTP', 'system1_vrf')
    system2_vrf = smtp_config.get('SMTP', 'system2_vrf')
    command = "show interfaces brief | display json"

    interfaces_json = execute_cli_command(command)
    interfaces = json.loads(interfaces_json)
    interfaces = interfaces["data"]["interfaces:interfaces"]["brief"]
    for interface in interfaces:
        if re.search("vni-0/\d*\.\d*", interface['name']):
            vrf = interface.get('vrf', "")
            if 'grt' in vrf or "WAN" in vrf or system1_vrf in vrf or system2_vrf in vrf:
                return vrf


def send_email(subject,  message):
    global smtp_config_file_path
    if os.path.exists(smtp_config_file_path):
        smtp_config = configparser.ConfigParser()
        smtp_config.read(smtp_config_file_path)
        recipients = "\"{}\"".format(smtp_config.get('SMTP', "recipients"))
        #server = socket.gethostbyname(smtp_config.get('SMTP', "server"))
        server = smtp_config.get('SMTP', "server")

        # script_path = Path(__file__).absolute()
        script_path = os.path.abspath(__file__)
        subject = "\"{}\"".format(subject)
        message = "\"{}\"".format(message)

        # create the vsmd filter
        filter_command = "\"test filter create type=v4 proto=6 dst=0.0.0.1/32 src={} sport={} control-app\"".format(
            server,
            smtp_config.getint("SMTP", "port")
        )
        command = "echo {} | nc -C 0 9001 > /dev/null 2>&1".format(filter_command)
        execute_process(command)

        # check to see if /etc/resolv.conf.augsave exists, if so backup
        resolv_conf_path = '/etc/resolv.conf.augsave'
        try:
            os.rename(resolv_conf_path, '{}.bak'.format(resolv_conf_path))
        except FileNotFoundError:
            pass
        os.symlink('/etc/netns/grt/resolv.conf.augsave', resolv_conf_path)

        # get the wan vrf name
        vrf = get_virtual_router_name()
        print("vrf: {}".format(vrf))
        # execute the script to send the email
        command = "ip netns exec {} {} --send-smtp {} {} {} {} {} {} {} {}".format(
            vrf,
            script_path,
            smtp_config.get('SMTP', "sender"),
            recipients,
            subject,
            server,
            smtp_config.getint("SMTP", "port"),
            smtp_config.get("SMTP", "user"),
            smtp_config.get("SMTP", "password"),
            message)

        # print(command)
        # execute_process(command)
        email_thread = threading.Thread(target=execute_process, args=(command,))
        email_thread.start()
        # cleanup /etc/resolv.conf.augsave
        # try:
        #     os.remove(resolv_conf_path)
        #     os.rename('{}.bak', resolv_conf_path.format(resolv_conf_path))
        # except FileNotFoundError:
        #     pass

        # delete the vsmd filter
        # filter = "test filter delete type=v4 proto=6 dst=0.0.0.1/32 src={} sport={} control-app".format(server, port)
        # command = "echo {}; | nc -c 0 9001 > /dev/null 2>&1".format(filter)
        # execute_process(command)


def send_smtp_message(sender, recipients, subject, server, port, user, password, message):
    # send the email
    # outfile = open("debug.txt", "+a")
    # outfile.write("sender: {} recipients: {} subject: {} server: {} port: {} user: {} password: {} message: {}".format(sender, recipients, subject, server, port, user, password, message))
    # outfile.close()
    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = recipients
    msg['Subject'] = subject
    msg.attach(MIMEText(message))

    server = socket.gethostbyname(server)

    retry = 5
    while retry:
        try:
            mailserver = smtplib.SMTP_SSL(server, port)
            mailserver.ehlo()

            mailserver.login(user, password)

            mailserver.sendmail(sender, recipients.split(","), msg.as_string())
        except Exception as e:
            time.sleep(1)
            retry = retry - 1
    # print('Email sent')


def create_rollback(timestamp = None):
    if not timestamp:
        timestamp = execution_time

    rollback_name = "restage_" + timestamp
    log('creating system rollback: {}'.format(rollback_name))
    command = 'request system create-snapshot description {} no-confirm '.format(rollback_name)
    result = execute_cli_command(command)
    if 'System state saved' in result:
        log("cli reports snapshot saved, testing if it's actually there")
        if get_rollback_timestamp(timestamp, returnLatestOnFail=False):
            log("successfully created rollback")
            return True
    log("creating rollback failed")
    return False


def get_rollback_timestamp(execution_time, returnLatestOnFail=False):
    log("get_rollback_timestamp: execution_time={}".format(execution_time))
    rollback_desc = "restage_" + execution_time
    snapshots_json = execute_cli_command('show system snapshots | display json')
    snapshots = json.loads(snapshots_json)
    snapshots = snapshots['data']['system:system']['snapshots']
    timestamp = None
    for s in snapshots:
        if s.get('description') == rollback_desc:
            timestamp = s.get('timestamp')
            break
    if not timestamp and not returnLatestOnFail:
        s = snapshots[0]
        timestamp = s.get('timestamp')
        log("rollback: {} not found! returning latest rollback named: {}".format(rollback_desc, s['description']))
    elif not timestamp and returnLatestOnFail:
        return False
    return timestamp


def rollback(timestamp, serial_number):
    log("rollback: {} {}".format(timestamp, serial_number))
    # try:
    #     hostname = socket.gethostname()
    #     send_email("restage.py: rolling back {}".format(hostname), "restage.py failed for {}, rolling back last known good config")
    # except Exception as e:
    #     log("could not get hostname due to exception: ".format(e))
    log_current_data()
    set_serial_number(serial_number)
    timestamp = get_rollback_timestamp(timestamp, returnLatestOnFail=True)
    command = 'request system rollback no-confirm to {}'.format(timestamp)
    result = execute_cli_command(command)


def is_ptvi_up():
    command = 'show interfaces brief | tab | grep ptvi'
    result = execute_cli_command(command)
    for r in result.split('\n'):
        if "up    up" in r and '-Control-VR' in r:
            return True
    return False


def rollback_if_needed(timestamp, check_function, pass_message, fail_message, serial_number):
    for x in range(60):
        if check_function():
            log(pass_message, terminal=False)
            return True
        time.sleep(10)
    log(fail_message, terminal=True)
    rollback(timestamp, serial_number)
    delete_scheduled_command()
    log("exiting after rollback")
    sys.exit()


def get_package_version():
    package_info_path = "/opt/versa/etc/package_info.in"
    with open(package_info_path) as f:
        lines = f.readlines()
        version = {}
        for line in lines:
            if "ver_major" in line or "ver_minor" in line or "ver_service" in line:
                token_list = line.replace("\'", "").replace("\n", "").replace("\t", "").replace(" ", "").replace(",","").split("=")
                version[token_list[0]] = token_list[1]
        if len(version) == 3:
            return "{}.{}.{}".format(version["ver_major"], version["ver_minor"], version["ver_service"])

        # legacy way of reading the file
        # if sys.version_info[0] == 2:
        #     f.seek(0)
        #     package_info = f.readlines()
        # else:
        #     package_info = lines
        f.seek(0)
        lines = f.readlines()
        for line in lines:
            if 'versa_pkg' in line:
                pkg_list = line.split('-')
                return pkg_list[len(pkg_list) - 1].split('\'')[0]

        f.close()
        return version


def reset_config():
    log('erasing device configuration')
    release = get_package_version()
    log("release = {}".format(release), terminal=True)
    if "21.2" in release:
        command = "request erase running-config"
        result = execute_cli_command(command)
    else:
        command = "/opt/versa/scripts/cdbclear.sh"
        result = execute_process(command)
    log(result)


def get_confirmation():
    cont_message_1 = "this script will reset the device and restage it using the passed paramerters.\n"
    cont_message_1 = "{}this will cause a service outage for the duration of the script\n".format(cont_message_1)
    cont_message_1 = "{}do you want to continue (y/n):  ".format(cont_message_1)
    log(cont_message_1, terminal=False)

    if sys.version_info[0] == 2:
        cont = raw_input(cont_message_1).rstrip()
        while (cont.lower() != 'y' and cont.lower() != 'n'):
            cont = raw_input(cont_message_1)
        if cont.lower() == 'n':
            log("received n, exiting")
            sys.exit()
        cont_message_2 = "Please confirm one last time, this will reset the device and restage using the pass parameters (y/n): "
        log(cont_message_2, terminal=False)
        cont = raw_input(cont_message_2).rstrip()
        while (cont.lower() != 'y' and cont.lower() != 'n'):
            cont = raw_input(cont_message_2)
        if cont.lower() == 'n':
            log("received n, exiting")
            sys.exit()
    else:
        cont = input(cont_message_1).rstrip()
        while (cont.lower() != 'y' and cont.lower() != 'n'):
            cont = input(cont_message_1)
        if cont.lower() == 'n':
            log("received n, exiting")
            sys.exit()
        cont_message_2 = "Please confirm one last time, this will reset the device and restage using the pass parameters (y/n): "
        log(cont_message_2, terminal=False)
        cont = input(cont_message_2).rstrip()
        while (cont.lower() != 'y' and cont.lower() != 'n'):
            cont = input(cont_message_2)
        if cont.lower() == 'n':
            log("received n, exiting")
            sys.exit()


def setup_logging(execution_time):
    log_filename = '{}_{}.log'.format("restage", execution_time)
    logging.basicConfig(filename=log_filename,
                        level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    owned_file_handler = OwnedFileHandler(log_filename, mode='a+',
                                          uid=getpwnam('versa').pw_uid,
                                          gid=grp.getgrnam('versa')[2])
    log = logging.getLogger()
    log.handlers.pop()
    log.addHandler(owned_file_handler)
    log.setLevel(logging.INFO)

    # redirect stderr into the log file
    sys.stderr = open(log_filename, 'a')

    logging.info("Starting Script version: {}".format(version))


def log(message, terminal=True):
    logging.info(message)
    if terminal:
        print(message)


def get_os_release_info():
    """determine whether this is trusty or bionic"""
    output = execute_process("lsb_release -a 2>/dev/null")
    release_info = {}
    if output:
        try:
            results = output.split('\n')
            for r in results:
                if r:
                    r = r.replace('\t', '')
                    key = r.split(":")[0]
                    value = r.split(":")[1]
                    release_info[key] = value

        except ValueError as e:
            log("Failed to get os release info")
    return release_info


def is_versa_services_running():
    log("Checking to see if Versa Services are running", terminal=True)
    codename = get_os_release_info()['Codename']
    if codename == "trusty":
        status = execute_process("service versa-flexvnf status").split('\n')
    elif codename == "bionic":
        status = execute_process('/opt/versa/scripts/initflexhelper.sh -a status').split('\n')
    else:
        log("unable to verify ubuntu version, returning false")
        return False
    if not status:
        log("")
        return False
    for s in status:
        if s and s.startswith("versa-") and not 'is Running' in s:
            log("versa services not running\n{}".format(s), terminal=True)
            return False
    log("all services running, continuing", terminal=True)
    return True


def verify_versa_services(timeout=300):
    '''Checks to see if Versa Services are running and attempts to restart them if not after <timeout> seconds'''
    log("verify_versa_services", terminal=False)
    if is_versa_services_running():
        log("versa services running, continuing", terminal=False)
        return True
    else:
        # services are not currently running , give it a swift kick in the ass
        log("versa services are not running, attempting restart".format(timeout),
            terminal=True)
        status = execute_process("service versa-flexvnf restart")

        # wait for the timeout value to see if all services start
        for x in range(timeout):
            if is_versa_services_running():
                log("versa services running, continuing", terminal=False)
                return True
            time.sleep(1)
        return False


def is_netconf_connected():
    log("Checking to see if a director netconf is established", terminal=True)
    status = execute_process("netstat -a | grep 2022 | grep ESTABLISHED")
    if status:
        log("netconf connected", terminal=True)
        return True
    else:
        log("netconf not connected", terminal=True)
        return False


def detect_vos_download():
    """Check to see if the director is performing a VOS upgrade"""
    global package_file_path, download_started, upgrade_started, download_detection_completed
    log_filename = '{}_{}.log'.format("restage", execution_time)
    start_time = os.path.getmtime(log_filename)
    log("detect vos download started")
    for x in range(300):  # check for 5 minutes to see if there is an update
        time.sleep(1)
        if upgrade_started:
            log("upgrade started, exiting download detection")
            download_detection_completed = True
            return
        for pf in os.listdir(packages_path):
            if pf.startswith('.versa-flexvnf-'):
                package_file_path = packages_path + pf
                mtime = os.path.getmtime(package_file_path)
                # print("\t{} mtime={}".format(pf, mtime))
                if mtime > execution_time:  # is the download file greater than the start time
                    download_started = True
                    # this is a file that was created after the start of execution
                    log('Download of VersaOS in progress: {}'.format(package_file_path))
                    # send_email(subject="restage.py - {} Download of VersaOS in progress".format(socket.gethostname()),
                    #            message="restage.py {} Download of VersaOS in progress: {}".format(socket.gethostname(),
                    #                                                                               package_file_path))
                    download_detection_completed = True
                    return
    package_file_path = ""
    download_started = False
    download_detection_completed = True
    return


def handle_vos_download():
    # if we are here then we have determined if there is a download in process, if so then check to see that it finishes
    #  within 30 minutes
    global package_file_path, download_started, download_completed, upgrade_started
    if download_started and package_file_path:
        package_file_name = os.path.split(package_file_path)[-1][1:-7]
        LINE_UP = '\033[1A'
        LINE_CLEAR = '\x1b[2K'
        for x in range(1800):  # 30 minutes
            try:
                log("{} {} bytes".format(package_file_name, os.path.getsize(package_file_path)))
            except FileNotFoundError:
                pass

            if package_file_name in os.listdir(packages_path):
                log('Download of {} completed'.format(package_file_path))
                # send_email(subject="restage.py - {} Download of VersaOS in complete".format(socket.gethostname()),
                #            message="restage.py {} Download of VersaOS {} complete".format(socket.gethostname(),
                #                                                                           package_file_name))
                download_completed = True
                break
            time.sleep(1)
            # log(LINE_UP, end=LINE_CLEAR)

        if not download_completed:
            log('Download of {} failed, initiating rollback'.format(package_file_path))
            # kill all staging processes
            # reset device config
            # initiate rollback
            # send_email(subject="restage.py - {} Download of VersaOS has failed".format(socket.gethostname()),
            #            message="restage.py {} Download of VersaOS {} failed".format(socket.gethostname(),
            #                                                                         package_file_name))
            return


def detect_vos_upgrading():
    global package_file_path, download_started, download_completed, upgrade_started, upgrade_detection_completed
    log("Checking to see if software upgrade is in progress, this will take 2 minutes")
    try:
        os.rename(upgrade_log_path, "{}.{}".format(upgrade_log_path, execution_time))
        log("renamed existing upgrade log to {}.{}".format(upgrade_log_path, execution_time))
    except FileNotFoundError:
        pass

    x = 0 # counter

    while not upgrade_detection_completed:
        time.sleep(1)
        if download_started and not download_completed:
            x = 0  # reset the counter to 0so we have 120s after the upgrade completes
        if os.path.exists(upgrade_log_path):
            log("upgrade started")
            upgrade_detection_completed = True
            upgrade_started = True
            return
        x = x + 1
        if x == 300:
            upgrade_detection_completed = True
    return


def handle_software_upgrade():
    """detect and handle any software upgrades"""
    # delete staging monitor time of 10 minutes
    # schedule rollback for 60 minutes
    upgrade_log = open(upgrade_log_path)
    from_version = to_version = ""
    for x in range(1800):  # timeout of 30 minutes
        time.sleep(1)
        # detect upgrade version
        # if version requires reboot then schedule cron
        if not from_version:
            upgrade_log.seek(0)
            lines = upgrade_log.readlines()
            for line in lines:
                if re.search("Initiating upgrade from", line):
                    upgrade_version_list = line.split()
                    from_version = line.split()[6].split("-")[-1][0:-4]
                    to_version = line.split()[-1].split("-")[-1][0:-4]
                    # send_email(subject="restage.py - Upgrade of VersaOS from {} to {} in progress".format(from_version, to_version),
                    #     message="restage.py - Upgrade of VersaOS from {} to {} in progress".format(from_version, to_version))
                    break
        # seek the begging of the file
        upgrade_log.seek(0)
        # read last line of file
        lines = upgrade_log.readlines()
        for line in lines[-5:]:
            if re.search("DOWNGRADE task ended", line) or re.search("UPGRADE to versa-flexvnf-.*bin succeeded\\n",
                                                                    line):
                # send_email(subject="restage.py - Upgrade of VersaOS has been completed",
                #            message="restage.py upgrad of VersaOS has been completed")
                return

    # if I am here upgrade has failed to complete within the 30 minutes of time, blow chucks
    # this is where I would do a rollback
    # send failure blah blah blah


def get_current_serial_number():
    """get the serial number of the device"""
    serial_number = open(serial_file_path).read().rstrip()
    log("current serial number: {}".format(serial_number))
    return serial_number


def set_serial_number(serial_number):
    """set the serial number to the device"""

    if len(serial_number) < 1:
        log("Unable to set serial number: length must be > 1 characters")
        return False
    elif len(serial_number) > 255:
        log("Unable to set serial number: length must be < 255 characters")
        return False
    elif not re.match("^[a-zA-Z0-9][a-zA-Z0-9_-]+$", serial_number):
        log("Serial number can only have alphanumerics, hyphens, dots and underscores")
        return False

    # delete the current serial number
    log("setting serial number to {}".format(serial_number))
    uid = os.stat(serial_file_path).st_uid
    gid = os.stat(serial_file_path).st_gid

    os.unlink(serial_file_path)
    serial_number_file = open(serial_file_path, "w")
    serial_number_file.write(serial_number)
    serial_number_file.close()
    os.chmod(serial_file_path, 0o444)
    os.chown(serial_file_path, uid, gid)
    return True

def ping_default_gw():
    """pings the default gateway is reachable"""
    try:
        gateway = ""
        xml_out = execute_cli_command('show route routing-instance WAN1-Transport-VR 0.0.0.0/0 exact | display xml')
        xml_list = xml_out.split("\n")
        xml_list.pop(0)
        xml_list.pop(0)
        xml_list.pop()
        myroot = ET.fromstring("\n".join(xml_list))
        children = myroot.getchildren()[0].getchildren()[0].getchildren()[0].getchildren()
        for child in children:
            if "routeRibPrefixTemplateList" in child.tag:
                route_info = child.getchildren()
                for route_element in route_info:
                    if "gatewayAddress" in route_element.tag:
                        gateway = route_element.text
                        break
        log("ping {} routing-instance WAN1-Transport-VR count 10".format(gateway))
        if gateway:
            execute_cli_command("ping {}".format(gateway))
    except Exception as e:
        log("unable log the ping of the default gateway due to exception: {}".format(e))

def log_current_data():
    """get a bunch of information prior to rolling back the configuration"""
    log("\n\n*****start data dump due before rollback*****")
    log("args: {}\n".format(args))
    execute_cli_command("show system details")
    execute_cli_command("show interfaces brief | tab")
    execute_cli_command("show orgs org-services Versa-Provider ipsec vpn-profile branch ike history")
    execute_cli_command("show  netconf-state sessions")
    execute_cli_command("")
    ping_default_gw()
    log("*****end data dump before rollback*****")


def passtwo(p2_args):
    """
    :param p2_args:
    :return:
    1. use the staging script to check if the passed parameters are valid, if not abort
    2. use the staging script to generate the configuration
    3. use the staging script to generate the serial number
    4. use the staging script to check for confd
    5. use the staging script to load the staging configuration
    6. schedule passthree to rollback in the case of an error
    """
    global g_command
    arg_data = deserialize(p2_args)
    setup_logging(arg_data['execution_time'])
    log("we are in pass two!!!!", terminal=False)

    log("\ntypeof arg_data['args']: {}".format(type(arg_data['args'])))
    log("\npass 2 arg_data: {}\n".format(arg_data))
    log("\n")

    # wait for versa services to be running, blow chunks if they aren't
    log("waiting for versa services", terminal=False)
    services_running = False
    for x in range(2): # 2 attempts == up to 10 minutes
        if verify_versa_services():
            services_running = True
            break
    if not services_running:
        # todo: should I capture error stats and such here?
        log("extreme failure, Versa services will not start")
        sys.exit(-1)

    serial_number = arg_data['args'].serial_number
    log("\nserial number: {}".format(serial_number))
    if serial_number:
        log("setting serial number to: {}".format(serial_number))
        set_serial_number(serial_number)
    else:
        serial_number = get_current_serial_number()

    log("checking prerequisites")
    staging_script.check_prereq()
    log("generating configurations")
    staging_script.generate_cfg(arg_data["args"])
    log("generating serial number")
    staging_script.generate_serial(serial_number)
    log("checking to see if the confd service is up")
    if 'check_confd_up' in dir(staging_script):
        log("checking to see if the confd service is up")
        staging_script.check_confd_up()
    try:
        # this is for the pyinstaller
        x = sys._MEIPASS
        script_path = sys.executable
    except AttributeError:
        # this is for the standalone script
        script_path = "{} {}".format(sys.executable, os.path.realpath(__file__))
    g_command = "{} --passthree {}".format(script_path, p2_args)
    schedule_command(g_command, delay_minutes=21)
    log("loading configuration")
    staging_script.load_cfg()

    # note: if passtwo is successful then these checks will never happen due to the reset that occurs during staging
    # check to see if there is a ptvi connection, if one does not come up then rollback to the snapshot taken in passone
    log("checking for ptvi connection")
    rollback_if_needed(arg_data['execution_time'],
                       is_ptvi_up,
                       "ptvi interfaces are up, rollback cancelled",
                       "10 minutes have passed without connection to controllers, rolling back",
                       arg_data['original_serial_number'])

    # check to see if there is a netconf session back to the director
    log("checking for netconf connection")
    rollback_if_needed(arg_data['execution_time'],
                       is_netconf_connected,
                       "netconf connection with director is up, rollback cancelled",
                       "10 minutes have passed without netconf connection to director, rolling back",
                       arg_data['original_serial_number'])

def passone():
    """
    passone:
    1. Check to see if versa services are running, exit if not
    2. Check to see if netconf is up to the director
    3. encode the  parameters for use in passtwo
    4.a. schedule passtwo to run using the AT command
    4.b. if passtwo is successfully scheduled then create a rollback
    4.c. if passtwo is successfully scheduled erase the running configuration
    :return:
    """
    global execution_time, arg_data, x, g_command
    staging_script.validate_args(args)
    execution_time = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
    setup_logging(execution_time)
    if not is_versa_services_running():
        log("Versa services are not running, exiting", terminal=True)
        sys.exit(-1)
    if not is_netconf_connected():
        log("netconf to director is not connected, exiting", terminal=True)
        sys.exit(-1)
    log("passed args: {}".format(args), terminal=False)
    get_confirmation()
    arg_data = {}
    arg_data['execution_time'] = execution_time
    arg_data['args'] = args
    arg_data['original_serial_number'] = get_current_serial_number()
    serialized_data = serialize(arg_data)
    try:
        x = sys._MEIPASS
        script_path = sys.executable
    except AttributeError:
        script_path = "{} {}".format(sys.executable, os.path.realpath(__file__))

    g_command = "{} --passtwo {}".format(script_path, serialized_data)
    if schedule_command(g_command, delay_minutes=5):

        rollback_created = create_rollback()

        if rollback_created:
            reset_config()
            check_scheduled_command()


if __name__ == "__main__":
    if not os.geteuid() == 0:
        print("This script must be ran with root privileges, please run as sudo.  Exiting!")
        sys.exit(1)

    # outfile = open("debug.txt", "+a")
    # outfile.write("arv: {}".format(sys.argv))
    # outfile.close()

    # this is for sending email only
    # lucid - is email?
    if len(sys.argv) == 10 and "--send-smtp" in sys.argv:
        # print("received sys.argv sendmail: {}".format(*sys.argv[2:]))

        send_smtp_message(*sys.argv[2:])
        sys.exit()

    parser = staging_script.add_args()
    parser.description = 'erase device configuration and restage using pass parameters'
    parser.add_argument('--passtwo', required=False, help=argparse.SUPPRESS)
    parser.add_argument('--passthree', required=False, help=argparse.SUPPRESS)
    args = parser.parse_args()
    if not args.passtwo and not args.passthree:
        # no args are given, execute passone
        passone()
    else:
        if args.passtwo:
            # passtwo logs are given, execute passtwo
            # arg_data = deserialize(args.passtwo)
            # setup_logging(arg_data['execution_time'])
            # log("we are in pass two!!!!", terminal=False)
            # log("arg_data: {}".format(arg_data))
            # # wait for versa services to be running, blow chunks if they aren't
            # log("waiting for versa services", terminal=False)
            # services_running = False
            # for x in range(2):
            #     if verify_versa_services():
            #         services_running = True
            #         break
            # if not services_running:
            #     log("extreme failure, Versa services will not start")
            #     sys.exit(-1)
            passtwo(args.passtwo)
        else:
            # passthree logs are given, execute passthree
            arg_data = deserialize(args.passthree)
            setup_logging(arg_data['execution_time'])
            log("we are in pass three!!!!", terminal=False)
            log("arg_data: {}".format(arg_data))
            # wait for versa services to be running, blow chunks if they aren't
            log("waiting for versa services", terminal=False)
            if not verify_versa_services(300):
                log("extreme failure, Versa services will not start")
                sys.exit(-1)
            rollback(arg_data['execution_time'], arg_data['original_serial_number'])