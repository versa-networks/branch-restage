#!/usr/bin/env python

#
# Utility script to setup pre/post-staging configuration
# on a branch device. This can be used both on VM and Lanner.
# On a VM, the script also generates a serial number which
# is used during branch-connect
#
# @author: mmehra@versa-networks.com
#

# Suppress DeprecationWarning
import re
import os
import grp
import pwd
import sys
import stat
import uuid
import socket
import argparse
import ipaddress
from collections import namedtuple
from email.utils import parseaddr
from fabric.api import local, hide, warn_only
import warnings
warnings.filterwarnings(action='ignore', module='.*cryptography.*')


STAGING_CFG = '/opt/versa/scripts/staging.cfg'
CONFD_LOAD = '/opt/versa/confd/bin/confd_load'
SERIAL_FILE = '/var/lib/vs/.serial'
DUAL_STACK = 0
SNAPPER = '/opt/versa/scripts/snapshot.sh'
PRESTAGE_SNAP = 'BRANCH-PRESTAGING'
LL_GWADDR = False
staging = namedtuple('Staging', 'site_type vpn_type')
prestaging = staging('branch-prestaging',
                     'branch-prestaging-sdwan')
poststaging = staging('branch-poststaging',
                      'branch-staging-sdwan')

site_type_map = {
    'prestaging': prestaging,
    'staging': poststaging,
}

# Color codes
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
BOLD = '\033[1m'
ENDC = '\033[0m'


# ---------------- TEMPLATES START --------------------

CIRCUIT46_SUB_TEMPLATE = '''
                    inet {
                        circuit-name BBAND;
                    }
                    inet6 {
                        circuit-name BBAND6;
                    }'''

CIRCUIT_SUB_TEMPLATE = '''
                    %(family)s {
                        circuit-name %(name)s;
                    }'''

ADDR_S4_SUB_TEMPLATE = '''
                inet {
                    address %(addr)s;
                } '''

ADDR_D4_SUB_TEMPLATE = '''
                inet {
                    dhcp;
                } '''

ADDR_S6_SUB_TEMPLATE = '''
                inet6 {
                    address %(addr6)s;
                } '''

ADDR_D6_SUB_TEMPLATE = '''
                inet6 {
                    dhcp {
                        client-ia-type {
                            ia-na;
                        }
                    }
                } '''

ADDR_SLAAC_SUB_TEMPLATE = '''
                inet6 {
                    mode host;
                } '''

TRANS_ADDR46_SUB_TEMPLATE = '''
                    addr1 {
                        %(family)s %(addr)s;
                    }
                    addr2 {
                        %(family6)s %(addr6)s;
                    }'''

TRANS_ADDR_SUB_TEMPLATE = '''
                    addr1 {
                        %(family)s %(addr)s;
                    }'''

ROUTE46_SUB_TEMPLATE = '''
        routing-options {
            static {
                route {
                    0.0.0.0/0 %(gateway)s none;
                    ::0/0 %(gateway6)s %(interface)s;
                }
            }
        }'''

ROUTE_SUB_TEMPLATE = '''
        routing-options {
            static {
                route {
                    %(nexthop)s %(gateway)s %(interface)s;
                }
            }
        }'''

ADDR_OBJECT = '''
       objects {
           addresses {
               addr1 {
                   fqdn %(addr)s;
               }
           }
       }'''

ADDR_OBJECTV46 = '''
       objects {
           addresses {
               addr1 {
                   fqdn %(addr1)s;
               }
               addr2 {
                   fqdn %(addr2)s;
               }
           }
       }'''

ORG_TEMPLATE = '''\
orgs {
    org Versa-Provider {
        type exclusive;
        appliance-owner;
        available-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR ];
        owned-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR];
        traffic-identification {
            using [ tvi-0/1.0 ];
            using-networks [ WAN-Network-1 ];
        }
        sd-wan {
            site {
                global-tenant-id %(tenant-id)s;
                site-name BRANCH1;
                management-routing-instance Versa-Provider-Control-VR;
                wan-interfaces {
                    vni-0/%(wan-port)s.%(vlan)s;
                }
            }
            controllers {
                controller1;
            }
        }
        available-service-node-groups [ default-sng ];
    }
    org-services Versa-Provider {
        ipsec {
            vpn-profile branch {
                vpn-type %(staging)s;
                local-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(local-psk)s";
                    id-string "%(local-id)s";
                }
                local {
                    interface-name vni-0/%(wan-port)s.%(vlan)s;
                }
                routing-instance WAN1-Transport-VR;
                tunnel-routing-instance Versa-Provider-Control-VR;
                tunnel-initiate automatic;
                ipsec {
                    life {
                        duration 25000;
                    }
                }
                ike {
                    group mod19;
                    lifetime 27000;
                    dpd-timeout 10;
                }
                peer-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(remote-psk)s";
                    id-string "%(remote-id)s";
                }
                peer {
                    %(ipsec-peer)s;
                }
                tunnel-interface tvi-0/1.0;
            }
        }
        %(addr-object)s
    }
}
service-node-groups {
    default-sng {
        services [ sdwan ];
    }
}'''


SYSTEM_TEMPLATE = '''
system {
    vnf-manager {
        vnf-mgmt-interfaces [ tvi-0/1.0 ];
    }
    identification {
        name versa-flexvnf;
    }
    services {
        ssh enabled;
        www enabled;
    }
    sd-wan {
        site {
            site-type %(site-type)s;
            chassis-id auto;
            provider-org Versa-Provider;
            wan-interfaces {
                vni-0/%(wan-port)s.%(vlan)s {
                    %(circuit)s
                }
            }
        }
        controllers {
            controller1 {
                site-name CONTROLLER1;
                site-id 1;
                transport-addresses {
                %(transport)s
                }
            }
        }
    }
}'''


NETWORK_TEMPLATE = '''
networks {
    WAN-Network-1 {
        interfaces [ vni-0/%(wan-port)s.%(vlan)s ];
    }
}
interfaces {
    vni-0/%(wan-port)s {
        ether-options {
            link-mode  %(link-mode)s;
            link-speed %(link-speed)s;
    }
	%(wwan_cfg)s
        unit %(vlan)s {
            %(vlan-id)s
            family {
            %(addr)s
            }
        }
    }
    tvi-0/1 {
        type ipsec;
        unit 0 {
            family {
            }
        }
    }
}
routing-instances {
    WAN1-Transport-VR {
        instance-type virtual-router;
        networks [ WAN-Network-1 ];
        %(route)s
    }
    Versa-Provider-Control-VR {
        instance-type virtual-router;
        interfaces [ tvi-0/1.0 ];
    }
}
protocols{
    lldp {
        enable true;
    }
}'''

ORG_TEMPLATE_PPPoE = '''\
orgs {
    org Versa-Provider {
        type exclusive;
        appliance-owner;
        available-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR ];
        owned-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR];
        traffic-identification {
            using [ tvi-0/1.0 tvi-0/3001.0];
        }
        sd-wan {
            site {
                global-tenant-id %(tenant-id)s;
                site-name BRANCH1;
                management-routing-instance Versa-Provider-Control-VR;
                wan-interfaces {
                    tvi-0/3001.0;
                }
            }
            controllers {
                controller1;
            }
        }
        available-service-node-groups [ default-sng ];
    }
    org-services Versa-Provider {
        ipsec {
            vpn-profile branch {
                vpn-type %(staging)s;
                local-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(local-psk)s";
                    id-string "%(local-id)s";
                }
                local {
                    interface-name tvi-0/3001.0;
                }
                routing-instance WAN1-Transport-VR;
                tunnel-routing-instance Versa-Provider-Control-VR;
                tunnel-initiate automatic;
                ipsec {
                    life {
                        duration 25000;
                    }
                }
                ike {
                    group mod19;
                    lifetime 27000;
                    dpd-timeout 10;
                }
                peer-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(remote-psk)s";
                    id-string "%(remote-id)s";
                }
                peer {
                    %(ipsec-peer)s;
                }
                tunnel-interface tvi-0/1.0;
            }
        }
        %(addr-object)s
    }
}
service-node-groups {
    default-sng {
        services [ sdwan ];
    }
}'''


SYSTEM_TEMPLATE_PPPoE = '''
system {
    vnf-manager {
        vnf-mgmt-interfaces [ tvi-0/1.0 ];
    }
    services {
        ssh enabled;
        www enabled;
    }
    sd-wan {
        site {
            site-type %(site-type)s;
            chassis-id auto;
            provider-org Versa-Provider;
            wan-interfaces {
                tvi-0/3001.0 {
                    %(circuit)s
                }
            }
        }
        controllers {
            controller1 {
                site-name CONTROLLER1;
                site-id 1;
                transport-addresses {
                %(transport)s
                }
            }
        }
    }
}'''


NETWORK_TEMPLATE_PPPoE = '''
interfaces {
    vni-0/%(wan-port)s {
        pppoe-base-interface;
        unit %(vlan)s {
            %(vlan-id)s
        }
    }
    tvi-0/3001 {
        type pppoe;
        pppoe {
            %(pppoe_service)s
            %(pppoe_access_concentrator)s
            username            %(pppoe_user)s;
            password            %(pppoe_password)s;
            vni-interface       vni-0/%(wan-port)s.%(vlan)s;
            lcp-echo-interval   10;
            lcp-echo-failure    2;
        }
        unit 0 {
            family {
            }
        }
    }
    tvi-0/1 {
        type ipsec;
        unit 0 {
            family {
            }
        }
    }
}
routing-instances {
    WAN1-Transport-VR {
        instance-type virtual-router;
        interfaces    [ tvi-0/3001.0 ];
    }
    Versa-Provider-Control-VR {
        instance-type virtual-router;
        interfaces [ tvi-0/1.0 ];
    }
}
protocols{
    lldp {
        enable true;
    }
}'''
# ---------------- TEMPLATES END --------------------


# ------------------ DSL TEMPLATE START-------------------

ORG_TEMPLATE_DSL = '''\
orgs {
    org Versa-Provider {
        type exclusive;
        appliance-owner;
        available-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR ];
        owned-routing-instances [ WAN1-Transport-VR Versa-Provider-Control-VR];
        traffic-identification {
            using [ tvi-0/1.0 dsl-0/0.0];
        }
        sd-wan {
            site {
                global-tenant-id %(tenant-id)s;
                site-name BRANCH1;
                management-routing-instance Versa-Provider-Control-VR;
                wan-interfaces {
                    dsl-0/0.0;
                }
            }
            controllers {
                controller1;
            }
        }
        available-service-node-groups [ default-sng ];
    }
    org-services Versa-Provider {
        ipsec {
            vpn-profile branch {
                vpn-type %(staging)s;
                local-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(local-psk)s";
                    id-string "%(local-id)s";
                }
                local {
                    interface-name dsl-0/0.0;
                }
                routing-instance WAN1-Transport-VR;
                tunnel-routing-instance Versa-Provider-Control-VR;
                tunnel-initiate automatic;
                ipsec {
                    life {
                        duration 25000;
                    }
                }
                ike {
                    group mod19;
                    lifetime 27000;
                    dpd-timeout 10;
                }
                peer-auth-info {
                    auth-type psk;
                    id-type email;
                    key "%(remote-psk)s";
                    id-string "%(remote-id)s";
                }
                peer {
                    %(ipsec-peer)s;
                }
                tunnel-interface tvi-0/1.0;
            }
        }
        %(addr-object)s
    }
}
service-node-groups {
    default-sng {
        services [ sdwan ];
    }
}'''

SYSTEM_TEMPLATE_DSL = '''
system {
    vnf-manager {
        vnf-mgmt-interfaces [ tvi-0/1.0 ];
    }
    services {
        ssh enabled;
        www enabled;
    }
    sd-wan {
        site {
            site-type %(site-type)s;
            chassis-id auto;
            provider-org Versa-Provider;
            wan-interfaces {
                dsl-0/0.0 {
                    %(circuit)s
                }
            }
        }
        controllers {
            controller1 {
                site-name CONTROLLER1;
                site-id 1;
                transport-addresses {
                %(transport)s
                }
            }
        }
    }
}'''

NETWORK_TEMPLATE_DSL = '''
interfaces {
    dsl-0/0 {
        pppoe {
            %(pppoe_service)s
            %(pppoe_access_concentrator)s
            username            %(pppoe_user)s;
            password            %(pppoe_password)s;
            lcp-echo-interval   5;
            lcp-echo-failure    3;
        }
        unit 0 {
            family {
            }
        }
        xdsl {
            vci       %(vci)s;
            vpi       %(vpi)s;
            multiplex %(multiplex)s;
            vlan-tag  %(vlan-tag)s;
        }
    }
    tvi-0/1 {
        type ipsec;
        unit 0 {
            family {
            }
        }
    }
}
routing-instances {
    WAN1-Transport-VR {
        instance-type virtual-router;
        interfaces    [ dsl-0/0.0 ];
    }
    Versa-Provider-Control-VR {
        instance-type virtual-router;
        interfaces [ tvi-0/1.0 ];
    }
}
protocols{
    lldp {
        enable true;
    }
}'''

# ---------------- TEMPLATES END --------------------


def add_args():
    parser = argparse.ArgumentParser(description='Setup branch staging config')
    parser.add_argument('-l', '--local-id',
                        help='Local id-string/email')
    parser.add_argument('-r', '--remote-id',
                        help='Remote id-string/email')
    parser.add_argument('-n', '--serial-number',
                        help='Serial number')
    parser.add_argument('-c', '--controller',
                        help='Controller IPv4 address/FQDN')
    parser.add_argument('-c6', '--controller6',
                        help='Controller IPv6 address/FQDN')
    parser.add_argument('-t', '--staging',
                        help='Staging type (default=staging)',
                        choices=[x for x in site_type_map.keys()],
                        default='staging')
    parser.add_argument('-w', '--wan-port',
                        help='WAN port number',
                        choices=[str(x) for x in range(0, 104)])
    parser.add_argument('-v', '--vlan',
                        help='VLAN id')
    parser.add_argument('-s', '--static',
                        help='Static IPv4/mask for WAN link')
    parser.add_argument('-s6', '--static6',
                        help='Static IPv6/mask for WAN link')
    parser.add_argument('-g', '--gateway',
                        help='Default gateway IP address')
    parser.add_argument('-g6', '--gateway6',
                        help='Default gateway IP address')
    parser.add_argument('-d', '--dhcp',
                        help='Use DHCP for WAN link',
                        action='store_true')
    parser.add_argument('-d6', '--dhcp6',
                        help='Use DHCPv6 for WAN link',
                        action='store_true')
    parser.add_argument('-a', '--slaac',
                        help='Use SLAAC for IPv6 WAN link',
                        action='store_true')
    parser.add_argument('-gt', '--global-tenant-id',
                        help='Global Tenant id')
    parser.add_argument('-lk', '--local-psk',
                        help='IPSec Key (default=1234)',
                        default='1234')
    parser.add_argument('-rk', '--remote-psk',
                        help='IPSec Key (default=1234)',
                        default='1234')
    parser.add_argument('-dsl', '--dsl_intf',
                        help='Use DSL interface for staging',
                        action='store_true')
    parser.add_argument('-vci', '--dsl_vci',
                        help='vci value,mandatory for DSL')
    parser.add_argument('-vpi', '--dsl_vpi',
                        help='vpi vlaue,mandatory for DSL')
    parser.add_argument('-multiplex', '--dsl_multiplex',
                        help='multiplex type, mandatory for DSL. multiplex type can either be llc or vcmux')
    parser.add_argument('-vtag', '--dsl_vlan_tag',
                        help='vlan id for dsl line')
    parser.add_argument('-p', '--pppoe',
                        help='Use PPPoE interface for staging',
                        action='store_true')
    parser.add_argument('-pu', '--pppoe_user',
                        help='PPPoE username,mandatory for PPPoE')
    parser.add_argument('-pp', '--pppoe-password',
                        help='PPPoE password,mandatory for PPPoE')
    parser.add_argument('-ps', '--pppoe-service',
                        help='PPPoE service name')
    parser.add_argument('-pa', '--pppoe-access-concentrator',
                        help='PPPoE access_concentrator')
    parser.add_argument('-wu', '--wwan_user',
                        help='wwan username')
    parser.add_argument('-wp', '--wwan-password',
                        help='wwan password')
    parser.add_argument('-wapn', '--wwan-apn',
                        help='wwan apn name')
    parser.add_argument('-wpin', '--wwan-pin',
                        help='wwan simpin'),
    parser.add_argument('-lmode', '--link-mode',
                        help='link mode for the interface [auto | half-duplex | full-duplex]',
                        choices=[str(x) for x in ["auto", "full-duplex", "half-duplex"]])
    parser.add_argument('-lspeed', '--link-speed',
                        help='link speed for the interface [auto |10m |100m |1g]',
                        choices=[str(x) for x in ["auto", "10m", "100m", "1g"]])
    return parser


def valid_email(email):
    if '@' in parseaddr(email)[1]:
        return True
    return False


def valid_fqdn(fqdn):
    if len(fqdn) > 255:
        print('Invalid length for FQDN %s' % (fqdn))
        return False

    # Strip exactly one dot from the right, if present
    if fqdn[-1] == ".":
        fqdn = fqdn[:-1]

    allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in fqdn.split("."))


def valid_addr(addr):
    try:
        socket.inet_pton(socket.AF_INET, addr)
        return True
    except socket.error:
        return False


def valid_addr6(addr):
    try:
        socket.inet_pton(socket.AF_INET6, addr)
        return True
    except socket.error:
        return False


def valid_addr_mask(addr_mask):
    if len(addr_mask.split('/')) != 2:
        return False

    addr, mask = addr_mask.split('/')
    if int(mask) < 1 or int(mask) > 31:
        print('Mask should be between [1-31]')
        return False

    try:
        socket.inet_pton(socket.AF_INET, addr)
        return True
    except socket.error:
        return False


def valid_addr6_mask(addr_mask):
    if len(addr_mask.split('/')) != 2:
        return False

    addr, mask = addr_mask.split('/')
    if int(mask) < 1 or int(mask) > 128:
        print('Mask should be between [1-128]')
        return False

    try:
        socket.inet_pton(socket.AF_INET6, addr)
        return True
    except socket.error:
        return False


def is_ll_addr6(addr):
    v6addr = ipaddress.IPv6Address(unicode(addr))
    return v6addr.is_link_local


def remove_quotes_escape(s):
    if (s[0] == s[-1]) and s.startswith(("'", '"')):
        s = s[1:-1]
    s = s.replace("\\", "")
    return s


def validate_args(args):
    if not(args.remote_id):
        print('Remote id is mandatory')
        sys.exit(1)

    if not (all(ord(char) < 128 for char in (args.remote_id))):
        print('Please input valid [ ASCII ] remote_id string')
        sys.exit(1)

    if not(args.local_id):
        print('Local id is mandatory')
        sys.exit(1)

    if not (all(ord(char) < 128 for char in (args.local_id))):
        print('Please input valid [ ASCII ] local_id string')
        sys.exit(1)

    if not(args.controller) and not(args.controller6):
        print('Controller IP is mandatory')
        sys.exit(1)

    if args.controller and args.controller6:
        global DUAL_STACK
        DUAL_STACK = 1

    if args.static and not(args.gateway):
        print('Default IPv4 gateway is mandatory when static option is used')
        sys.exit(1)

    if args.static6 and not(args.gateway6):
        print('Default IPv6 gateway is mandatory when static option is used')
        sys.exit(1)

    if not valid_email(args.remote_id):
        print('Invalid email %s' % (args.remote_id))
        sys.exit(1)

    if not valid_email(args.local_id):
        print('Invalid email %s' % (args.local_id))
        sys.exit(1)

    if args.controller and not valid_addr(args.controller) and \
            not valid_fqdn(args.controller):
        print('Invalid controller IPv4/FQDN %s' % (args.controller))
        sys.exit(1)

    if args.controller6 and not valid_addr6(args.controller6) and \
            not valid_fqdn(args.controller6):
        print('Invalid controller IPv6/FQDN %s' % (args.controller6))
        sys.exit(1)

    if args.static and args.dhcp:
        print('Static IP not applicable when DHCP option is specified')
        sys.exit(1)

    if args.dhcp and args.gateway:
        print('Gateway not applicable when DHCP option is specified')
        sys.exit(1)

    if args.dhcp6 and args.static6:
        print('Static Ipv6 not applicable when IPv6 DHCP option is specified')
        sys.exit(1)

    if args.dhcp6 and args.gateway6:
        print('Gateway not applicable when IPv6 DHCP option is specified')
        sys.exit(1)

    if args.static:
        if not valid_addr(args.gateway):
            print('Invalid gateway IP %s' % (args.gateway))
            sys.exit(1)

        if not valid_addr_mask(args.static):
            print('Invalid static IPv4/mask %s' % (args.static))
            sys.exit(1)

    if args.static6:
        if not valid_addr6(args.gateway6):
            print('Invalid gateway IP %s' % (args.gateway6))
            sys.exit(1)

        if not valid_addr6_mask(args.static6):
            print('Invalid static IPv6/mask %s' % (args.static6))
            sys.exit(1)

        if is_ll_addr6(args.gateway6):
            global LL_GWADDR
            LL_GWADDR = True

    if not(args.wan_port):
        args.wan_port = '0'

    if not(args.local_psk):
        args.local_psk = '1234'

    if not(args.remote_psk):
        args.remote_psk = '1234'

    if not(args.global_tenant_id):
        args.global_tenant_id = '1'

    if args.vlan:
        if not int(args.vlan) in range(1, 4095):
            print('Please input valid VLAN')
            sys.exit(1)
    else:
        args.vlan = 0
    if not args.link_mode:
        args.link_mode = 'auto'

    if not args.link_speed:
        args.link_speed = 'auto'

    # Remove any escape(\) or quotes(") since we are going
    # to quote these arguments when passing down to ConfD
    args.local_id = remove_quotes_escape(args.local_id)
    args.remote_id = remove_quotes_escape(args.remote_id)
    args.local_psk = remove_quotes_escape(args.local_psk)
    args.remote_psk = remove_quotes_escape(args.remote_psk)

    if DUAL_STACK:
        if (not(args.dhcp) and not(args.static) and not (args.pppoe) and not(args.dsl_intf)):
            print('IPv4 address/scheme needs to be configured for vni')
            sys.exit(1)

        if (not(args.dhcp6) and not(args.static6) and not(args.slaac)) and not (args.pppoe) and not(args.dsl_intf):
            print('IPv6 address/scheme needs to be configured for vni')
            sys.exit(1)
    else:
        if (args.controller and
                (not(args.dhcp) and not(args.static)) and not (args.pppoe) and not(args.dsl_intf)):
            print('IPv4 address/scheme needs to be configured for vni')
            sys.exit(1)

        if (args.controller6 and
                (not(args.dhcp6) and not(args.static6) and not(args.slaac))) and not (args.pppoe) and not(args.dsl_intf):
            print('IPv6 address/scheme needs to be configured for vni')
            sys.exit(1)
    if args.pppoe:
        if not args.pppoe_user or not args.pppoe_password:
            print(
                'PPPoE interface should be configured with madnatory Username(-pu),password(-pp)')
            sys.exit(1)
    if args.dsl_intf:
        if not args.dsl_vci or not args.dsl_vpi or not args.dsl_multiplex or not args.pppoe_user or not args.pppoe_password:
            print('DSL interface should be configured with madnatory vci(-vci),vpi(-vpi),dsl_multiplex(-dsl_multiplex),Username(-pu),password(-pp)')
            sys.exit(1)
    return


def get_custom_parameters(args):
    """
    Returns custom parameter based on the user input
    """

    global DUAL_STACK
    addr_object = ''
    route = ''
    vni_addr = ''
    interface = 'none'
    if LL_GWADDR:
        interface = 'vni-0/%s.%s' % (args.wan_port, args.vlan)

    if DUAL_STACK:
        circuit = CIRCUIT46_SUB_TEMPLATE

        family = family6 = 'fqdn'
        if valid_addr(args.controller):
            family = 'ip-address'
        if valid_addr6(args.controller6):
            family6 = 'ip6-address'

        if family == 'fqdn':
            ipsec_peer = 'peer-fqdn-list [ addr1 ]'
            addr_object = ADDR_OBJECT % ({'addr': args.controller})

        if family6 == 'fqdn':
            if family == 'fqdn':
                ipsec_peer = 'peer-fqdn-list [ addr1 addr2 ]'
                addr_object = ADDR_OBJECTV46 % ({'addr1': args.controller,
                                                 'addr2': args.controller6})
            else:
                ipsec_peer = 'peer-fqdn-list [ addr1 ]'
                addr_object = ADDR_OBJECT % ({'addr': args.controller6})

        if family == 'ip-address' and family6 == 'ip6-address':
            ipsec_peer = 'address [ %s %s ]' % (
                args.controller, args.controller6)

        transport = TRANS_ADDR46_SUB_TEMPLATE % ({'family': family,
                                                  'family6': family6,
                                                  'addr': args.controller,
                                                  'addr6': args.controller6})
        if args.dhcp:
            vni_addr = ADDR_D4_SUB_TEMPLATE
        if args.dhcp6:
            vni_addr = + ADDR_D6_SUB_TEMPLATE
        if args.slaac:
            vni_addr = + ADDR_SLAAC_SUB_TEMPLATE
        if args.static6:
            vni_addr = + ADDR_S6_SUB_TEMPLATE % ({'addr6': args.static6})
            route = ROUTE_SUB_TEMPLATE % ({'nexthop': '::0/0',
                                           'gateway': args.gateway6,
                                           'interface': interface})
        if args.static:
            vni_addr = + ADDR_S4_SUB_TEMPLATE % ({'addr':   args.static})
            route = ROUTE_SUB_TEMPLATE % ({'nexthop': '0.0.0.0/0',
                                           'gateway': args.gateway,
                                           'interface': 'none'})
        if args.static and args.static6:
            route = ROUTE46_SUB_TEMPLATE % ({'gateway': args.gateway,
                                             'gateway6': args.gateway6,
                                            'interface': interface})
        if args.pppoe:
            vni_addr = ''

    elif args.controller:
        circuit = CIRCUIT_SUB_TEMPLATE % ({'family': 'inet',
                                           'name': 'BBAND'})
        if valid_addr(args.controller):
            ipsec_peer = 'address [ %s ]' % (args.controller)
            family = 'ip-address'
        else:
            ipsec_peer = 'peer-fqdn-list [ addr1 ]'
            family = 'fqdn'
            addr_object = ADDR_OBJECT % ({'addr': args.controller})

        transport = TRANS_ADDR_SUB_TEMPLATE % ({'family': family,
                                                'addr': args.controller})
        if args.static:
            vni_addr = ADDR_S4_SUB_TEMPLATE % ({'addr':   args.static})
            route = ROUTE_SUB_TEMPLATE % ({'nexthop': '0.0.0.0/0',
                                           'gateway': args.gateway,
                                           'interface': 'none'})

        elif args.dhcp:
            vni_addr = ADDR_D4_SUB_TEMPLATE

        elif args.pppoe:
            vni_addr = ''

    elif args.controller6:
        circuit = CIRCUIT_SUB_TEMPLATE % ({'family': 'inet6',
                                           'name': 'BBAND6'})

        if valid_addr6(args.controller6):
            ipsec_peer = 'address [ %s ]' % (args.controller6)
            family = 'ip6-address'
        else:
            ipsec_peer = 'peer-fqdn-list [ addr1 ]'
            family = 'fqdn'
            addr_object = ADDR_OBJECT % ({'addr': args.controller6})

        transport = TRANS_ADDR_SUB_TEMPLATE % ({'family': family,
                                                'addr': args.controller6})
        if args.static6:
            vni_addr = ADDR_S6_SUB_TEMPLATE % ({'addr6':   args.static6})
            route = ROUTE_SUB_TEMPLATE % ({'nexthop': '::0/0',
                                           'gateway': args.gateway6,
                                           'interface': interface})

        if args.dhcp6:
            vni_addr += ADDR_D6_SUB_TEMPLATE

        if args.slaac:
            vni_addr += ADDR_SLAAC_SUB_TEMPLATE

        if args.pppoe:
            vni_addr = ''

    return (ipsec_peer, transport, route, circuit, vni_addr, addr_object)


def generate_cfg(args):
    print(' => Generating %s config' % (args.staging))
    tuple = site_type_map[args.staging]
    wwan_cfg = ''
    wwan = False

    # Get custom parameters
    ipsec_peer, transport, route, circuit, vni_addr, addr_object = \
        get_custom_parameters(args)

    if args.vlan:
        vlan_id = 'vlan-id %s;' % (args.vlan)
    else:
        vlan_id = ''

    if re.match(r"10[0-3]", args.wan_port):
        wwan = True

    if wwan:
        if args.wwan_user or args.wwan_password or args.wwan_apn or args.wwan_pin:
            wwan_cfg = 'wwan {\n'
            if args.wwan_apn:
                wwan_cfg += '		apn       '+args.wwan_apn+';\n'
            if args.wwan_pin:
                wwan_cfg += '		pin       '+args.wwan_pin+';\n'
            if args.wwan_user:
                wwan_cfg += '		username       '+args.wwan_user+';\n'
            if args.wwan_password:
                wwan_cfg += '		password       '+args.wwan_password+';\n'
            wwan_cfg += '	}'

    # Write config file
    cfg = open(STAGING_CFG, 'w')
    if args.pppoe:
        cfg.write(ORG_TEMPLATE_PPPoE % (
            {
                'tenant-id': args.global_tenant_id,
                'staging': tuple.vpn_type,
                'local-id': args.local_id,
                'remote-id': args.remote_id,
                'ipsec-peer': ipsec_peer,
                'local-psk': args.local_psk,
                'remote-psk': args.remote_psk,
                'addr-object': addr_object
            }))

        cfg.write(SYSTEM_TEMPLATE_PPPoE % (
            {
                'site-type': tuple.site_type,
                'circuit': circuit,
                'transport': transport,
            }))

        if args.pppoe_service:
            service_name = 'service-name       '+args.pppoe_service+';\n'
        else:
            service_name = ""
        if args.pppoe_access_concentrator:
            access_concentrator = 'access-concentrator        ' + \
                args.pppoe_access_concentrator+';\n'
        else:
            access_concentrator = ''

        cfg.write(NETWORK_TEMPLATE_PPPoE % (
            {
                'wan-port': args.wan_port,
                'vlan': args.vlan,
                'vlan-id': vlan_id,
                'pppoe_service': service_name,
                'pppoe_access_concentrator': access_concentrator,
                'pppoe_user': args.pppoe_user,
                'pppoe_password': args.pppoe_password,
            }))

    elif args.dsl_intf:
        cfg.write(ORG_TEMPLATE_DSL % (
            {
                'tenant-id': args.global_tenant_id,
                'staging': tuple.vpn_type,
                'local-id': args.local_id,
                'remote-id': args.remote_id,
                'ipsec-peer': ipsec_peer,
                'local-psk': args.local_psk,
                'remote-psk': args.remote_psk,
                'addr-object': addr_object
            }))

        cfg.write(SYSTEM_TEMPLATE_DSL % (
            {
                'site-type': tuple.site_type,
                'circuit': circuit,
                'transport': transport,
            }))

        if args.pppoe_service:
            service_name = 'service-name       '+args.pppoe_service+';\n'
        else:
            service_name = ""
        if args.pppoe_access_concentrator:
            access_concentrator = 'access-concentrator        ' + \
                args.pppoe_access_concentrator+';\n'
        else:
            access_concentrator = ''

        if not (args.dsl_vlan_tag):
            args.dsl_vlan_tag = 0

        cfg.write(NETWORK_TEMPLATE_DSL % (
            {
                'pppoe_service': service_name,
                'pppoe_access_concentrator': access_concentrator,
                'pppoe_user': args.pppoe_user,
                'pppoe_password': args.pppoe_password,
                'vci': args.dsl_vci,
                'vpi': args.dsl_vpi,
                'multiplex': args.dsl_multiplex,
                'vlan-tag': args.dsl_vlan_tag
            }))
    else:
        cfg.write(ORG_TEMPLATE % (
            {
                'tenant-id': args.global_tenant_id,
                'wan-port': args.wan_port,
                'vlan': args.vlan,
                'staging': tuple.vpn_type,
                'local-id': args.local_id,
                'remote-id': args.remote_id,
                'ipsec-peer': ipsec_peer,
                'local-psk': args.local_psk,
                'remote-psk': args.remote_psk,
                'addr-object': addr_object
            }))

        cfg.write(SYSTEM_TEMPLATE % (
            {
                'site-type': tuple.site_type,
                'wan-port': args.wan_port,
                'vlan': args.vlan,
                'circuit': circuit,
                'transport': transport,
            }))

        cfg.write(NETWORK_TEMPLATE % (
            {
                'wan-port': args.wan_port,
                'link-mode': args.link_mode,
                'link-speed': args.link_speed,
                'vlan': args.vlan,
                'vlan-id': vlan_id,
                'addr': vni_addr,
                'route': route,
                'wwan_cfg': wwan_cfg,
            }))

    cfg.close()

    # Change permission of generated cfg
    uid = pwd.getpwnam('admin').pw_uid
    gid = grp.getgrnam('versa').gr_gid
    os.chown(STAGING_CFG, uid, gid)

    print(' => Config file saved %s' % (STAGING_CFG))
    return


def generate_serial(serial):
    if not(serial):
        if os.path.exists(SERIAL_FILE):
            return
        print(' => Generating serial number')
        serial = str(uuid.uuid1())

    print(' => Saving serial number')
    fd = open(SERIAL_FILE, 'w+')
    fd.write(serial)
    fd.close()

    os.chown(SERIAL_FILE, pwd.getpwnam("versa").pw_uid,
             grp.getgrnam("versa").gr_gid)
    os.chmod(SERIAL_FILE,
             stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH)
    return


def warning(msg):
    print(WARNING + msg + ENDC)


def info(msg):
    print(OKBLUE + msg + ENDC)


def check_prereq():
    print(' => Checking if all required services are up')
    procs = ['confd', 'vmod', 'infmgr', 'rtd', 'rt-cli-xfm']
    for proc in procs:
        if not os.path.exists('/opt/versa/var/uptimes/up.versa-%s' % (proc)):
            warning('\n versa-%s not up. Please wait for it to come up' % (proc))
            info('\n Use "vsh status" to check process status')
            info(' Use "vsh restart" to restart services\n')
            sys.exit(1)

    print(' => Checking if there is any existing config')
    with warn_only():
        with hide('output', 'running', 'warnings'):
            res1 = local('%s -F c -p "/system/sd-wan/site/site-type"'
                         % (CONFD_LOAD),
                         capture=True)
            res2 = local('%s -F c -p "/interfaces/vni"' % (CONFD_LOAD),
                         capture=True)

            if len(res1) + len(res2) > 0:
                warning('\n Found existing config on system\n')
                info(' Please issue "request erase running-config" ')
                info(' from CLI and try staging after that\n')
                sys.exit(1)
    return


def check_confd_up():
    print(' => Check if control-plane is up and runnning')
    with warn_only():
        with hide('output', 'warnings', 'running'):
            res = local(
                '/sbin/iptables -C INPUT -p tcp --destination-port 2022 -j DROP')
            if res.succeeded:  # If drop rule is still on
                warning('\n All the required services do not seem to be running\n')
                info(' Please wait for a while, check "vsh status" and re-run the command')
                sys.exit(1)


def load_cfg():
    print(' => Loading generated config into CDB')
    with warn_only():
        with hide('running'):
            result = local('%s -lm -Fj %s' % (CONFD_LOAD, STAGING_CFG))
            if result.failed:
                warning('Error loading staging config file')
                return
    return


def take_snapshot(stage):
    if stage != 'prestaging':
        print('\n')
        return

    print(' => Taking prestaging snapshot\n')
    with warn_only():
        with hide('output', 'running'):
            result = local('%s create description "%s" internal' %
                           (SNAPPER, PRESTAGE_SNAP))
            if result.failed:
                warning('Error creating snapshot')
                return


def main():
    if os.geteuid() != 0:
        print('Please execute with sudo')
        sys.exit(0)

    parser = add_args()
    args = parser.parse_args()
    validate_args(args)

    print(' => Setting up %s config' % (args.staging))
    check_prereq()
    generate_cfg(args)
    generate_serial(args.serial_number)
    check_confd_up()
    load_cfg()
    take_snapshot(args.staging)
    return


if __name__ == '__main__':
    main()
